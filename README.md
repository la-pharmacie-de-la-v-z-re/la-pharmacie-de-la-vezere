La Pharmacie de la Vézère tire son nom de l’affluent de la Dordogne qui traverse le village de Montignac. L’officine, qui s’est toujours efforcée de fournir un service de la meilleure qualité qui soit, est fière d’être accréditée QMS Pharma 2010 (Quality Management System). Elle met également un point d’honneur à soigner son accessibilité, offrant à ses clients une surface de vente spacieuse et aérée de 300 m² et un grand parking, avec un accès handicapé.

https://www.pharmacie-vezere.com/promotions/

La Pharmacie de la Vézère vous accueille pour prendre soin de votre bien-être dans un cadre moderne et sécurisé. Que ce soit pour un simple conseil ou la prise en charge de cas médicaux plus complexes nécessitant diagnostic, traitement et suivi, nos pharmaciens sont là pour vous.

Sentez-vous comme chez vous dans notre pharmacie accueillante et accessible à tous avec un parking pour handicapés disponible ! L'espace commercial de grande taille garantit un accès généreux pendant les heures d'ouverture. Venez nous rendre visite aujourd'hui pour profiter de nos prix spéciaux sur les rouges à lèvres de luxe de Make Me Cosmetics, fabriqués à partir d'ingrédients naturels.

La Pharmacie de la Vézère est votre guichet unique pour tous vos besoins de santé. Venez nous rendre visite dès aujourd'hui pour avoir accès à des brassards de tension artérielle, des appareils de mesure pour diabétiques, et plus encore. Notre magasin a tout ce dont vous avez besoin pour prendre soin de toute la famille!

Si tout ce dont vous avez besoin est un petit quelque chose pour votre rhume ou votre hypoglycémie, passez à la pharmacie de la Vézère. Nous proposons également la mesure du poids et le contrôle de votre tension artérielle - venez nous voir dès aujourd'hui!

L'atmosphère chaleureuse de cette pharmacie familiale est idéale pour faire du shopping. Dès l'instant où vous entrez, vous avez droit à un large éventail de produits, tous adaptés à votre budget et à vos besoins. Le service à la clientèle dépasse les attentes avec un personnel de vente amical prêt à répondre à toutes les questions sur leur connaissance des produits. Passez nous voir dès aujourd'hui !

Nous vous proposons un éventail de services :

- Sur place : prise de tension, contrôle de la glycémie…
- Prise de poids et de mesure.
- Possibilité de peser son bébé gratuitement à la pharmacie.
- Entretiens avec un pharmacien pour mieux aborder certaines pathologies, (asthme, anticoagulants oraux…)
- Entretiens avec des spécialistes en micronutrition pour préserver votre santé naturellement et en douceur.
- Préparations et mélanges de plantes.
- Préparations et mélanges d’extraits de plantes fraiches.
- Préparations et conseils à base d’huiles essentielles.
- Envoi de médicaments par la poste.
- Livraison et installation à domicile de matériel médical.
